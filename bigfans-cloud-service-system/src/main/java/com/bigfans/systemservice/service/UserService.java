package com.bigfans.systemservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.systemservice.model.User;

public interface UserService extends BaseService<User> {
	
	/**
	 * 判断当前邮箱是否已经使用
	 * @param email
	 * @return
	 * @throws Exception 
	 */
	boolean emailExist(String email) throws Exception;
	
	/**
	 * 判断当前用户名是否存在
	 * @param account
	 * @return
	 * @throws Exception 
	 */
	boolean accountExist(String account) throws Exception;
	
	/**
	 * 注册用户
	 * @param user
	 * @return 
	 * @throws Exception 
	 */
	User regist(User user) throws Exception;
	
	/**
	 * 通过账号或者邮箱登陆
	 * @param account 账号或邮箱
	 * @param password
	 * @return
	 */
	User login(String account, String password) throws Exception;
	
	/**
	 * 通过账号或者邮箱登陆
	 * @param account 账号或邮箱
	 * @param password
	 * @param md5 是否进行md5加密
	 * @return
	 * @throws Exception 
	 */
	User login(String account, String password, boolean md5) throws Exception;
	
}
