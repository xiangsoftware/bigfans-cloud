package com.bigfans.framework;

import java.util.Map;

import com.bigfans.framework.model.SystemSetting;

public class Applications {
	
	private static Configuration configuration;
	
	private Applications() {
		configuration = Configuration.getInstance();
	}

	private static SystemSetting systemSetting;

	public static SystemSetting getSystemSetting() {
		return systemSetting;
	}

	public static void setSystemSetting(SystemSetting systemSetting) {
		Applications.systemSetting = systemSetting;
	}
	
	public static void putConfigurations(Map<String , Object> configs){
		configuration.put(configs);
	}

	public static Configuration getConfiguration() {
		return configuration;
	}

	public static ThreadLocal<CurrentUser> currentUserThreadLocal = new ThreadLocal<>();

	public static CurrentUser getCurrentUser(){
		return currentUserThreadLocal.get();
	}

	public static void setCurrentUser(CurrentUser currentUser) {
		currentUserThreadLocal.set(currentUser);
	}

	public static void clearCurrentUser(){
		currentUserThreadLocal.remove();
	}
}
