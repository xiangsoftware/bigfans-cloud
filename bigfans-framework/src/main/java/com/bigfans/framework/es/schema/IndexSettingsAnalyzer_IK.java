package com.bigfans.framework.es.schema;

import org.elasticsearch.common.settings.Settings.Builder;

public class IndexSettingsAnalyzer_IK implements ElasticSchema {
	
	public static final String NAME = "ik";

	@Override
	public void build(Builder settingsBuilder) {
		settingsBuilder.put("index.analysis.analyzer.ik.type", "custom");
		settingsBuilder.put("index.analysis.analyzer.ik.tokenizer", "ik_max_word");
	}
}
