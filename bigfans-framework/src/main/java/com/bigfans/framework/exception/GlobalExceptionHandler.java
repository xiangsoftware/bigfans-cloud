package com.bigfans.framework.exception;

import com.bigfans.framework.web.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author lichong
 * @create 2018-04-02 下午10:02
 **/
public class GlobalExceptionHandler {

    @ExceptionHandler(value = UserNotLoginException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public RestResponse loginExceptionHandler(UserNotLoginException ex){
        return RestResponse.error("user not login");
    }

    @ExceptionHandler(value = ServiceRuntimeException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public RestResponse serviceExceptionHandler(ServiceRuntimeException ex){
        return RestResponse.error(ex.getErrorCode() , ex.getMessage());
    }
}
