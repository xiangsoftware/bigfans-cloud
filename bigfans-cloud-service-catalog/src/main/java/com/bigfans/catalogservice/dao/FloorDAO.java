package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Floor;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong 2015年5月14日上午11:05:06
 *
 */
public interface FloorDAO extends BaseDAO<Floor> {

	List<Floor> listFloor();

	int insertFloor(Floor example);

}
