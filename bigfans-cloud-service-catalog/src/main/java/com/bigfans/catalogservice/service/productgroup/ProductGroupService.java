package com.bigfans.catalogservice.service.productgroup;

import com.bigfans.catalogservice.model.Product;
import com.bigfans.catalogservice.model.ProductGroup;
import com.bigfans.catalogservice.model.ProductGroupAttribute;
import com.bigfans.catalogservice.model.ProductGroupTag;
import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.model.PageBean;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 商品组业务类
 * @author lichong 
 * @date 2015年9月16日 上午10:36:05 
 * @version V1.0
 */
public interface ProductGroupService extends BaseService<ProductGroup>{
	
	PageBean<ProductGroup> pageByCategory(String catId , Long start,Long pagesize) throws Exception;
	
	void create(ProductGroup pg , List<Product> products , List<ProductGroupAttribute> pgAttrs) throws Exception;

	List<ProductGroupAttribute> listAttributeById(String productGroupId) throws Exception;

	void create(ProductGroup pg, List<Product> products, List<ProductGroupAttribute> pgAttrs, List<ProductGroupTag> tags)
			throws Exception;

	String getDescription(String pgId , String prodId);
}
