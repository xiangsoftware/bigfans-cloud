package com.bigfans.catalogservice.model;

import com.bigfans.framework.utils.CollectionUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 商品SKU结果
 * 
 * @author lichong
 *
 */
@Data
public class SkuResult implements Serializable {

	private static final long serialVersionUID = 7399149219218538867L;

	private Map<String, Set<String>> skuMap;
	private SKU selectedSku;
	private Set<String> disabledValIdList;
	private List<SpecGroup> specGroups;
	
	public Boolean getHasSku() {
		return CollectionUtils.isNotEmpty(skuMap);
	}

}
