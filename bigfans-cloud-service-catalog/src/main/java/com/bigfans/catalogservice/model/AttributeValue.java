package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.AttributeValueEntity;
import lombok.Data;

@Data
public class AttributeValue extends AttributeValueEntity {

	private static final long serialVersionUID = -495340455594785736L;

	private String optionName;

	@Override
	public String toString() {
		return "AttributeValue [optionId=" + optionId + ", value=" + value + ", id=" + id + "]";
	}

}
