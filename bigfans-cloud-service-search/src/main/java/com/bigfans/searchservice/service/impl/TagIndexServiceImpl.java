package com.bigfans.searchservice.service.impl;

import com.bigfans.framework.es.ElasticTemplate;
import com.bigfans.framework.es.request.CreateIndexCriteria;
import com.bigfans.framework.es.request.CreateMappingCriteria;
import com.bigfans.framework.es.schema.DefaultIndexSettingsBuilder;
import com.bigfans.searchservice.schema.mapping.TagMapping;
import com.bigfans.searchservice.service.TagIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(TagIndexServiceImpl.BEAN_NAME)
public class TagIndexServiceImpl implements TagIndexService {

    public static final String BEAN_NAME = "tagIndexService";

    @Autowired
    private ElasticTemplate elasticTemplate;

    public void create() throws Exception {
        this.createIndex();
        this.createMapping();
    }

    public void createIndex() throws Exception {
        elasticTemplate.deleteIndex(TagMapping.INDEX);
        if (!elasticTemplate.isIndexExists(TagMapping.INDEX)) {
            CreateIndexCriteria criteria = new CreateIndexCriteria();
            criteria.setIndexName(TagMapping.INDEX);
            criteria.setAlias(TagMapping.ALIAS);
            criteria.setVersion(1L);
            criteria.setSettingsBuilder(new DefaultIndexSettingsBuilder());
            elasticTemplate.createIndex(criteria);
        }
    }

    public void createMapping() throws Exception {
        CreateMappingCriteria action = new CreateMappingCriteria();
        action.setMappingBuilder(new TagMapping());
        action.setIndexName(TagMapping.INDEX);
        action.setType(TagMapping.TYPE);
        elasticTemplate.createMapping(action);
    }

}
