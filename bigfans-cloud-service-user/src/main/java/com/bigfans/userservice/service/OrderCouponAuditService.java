package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.userservice.model.OrderCouponAudit;

public interface OrderCouponAuditService extends BaseService<OrderCouponAudit>{

    OrderCouponAudit getByOrder(String orderId) throws Exception;


}
