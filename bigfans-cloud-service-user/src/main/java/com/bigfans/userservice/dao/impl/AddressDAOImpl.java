package com.bigfans.userservice.dao.impl;


import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.userservice.dao.AddressDAO;
import com.bigfans.userservice.model.Address;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository(AddressDAOImpl.BEAN_NAME)
public class AddressDAOImpl extends MybatisDAOImpl<Address> implements AddressDAO {

	public static final String BEAN_NAME = "addressDAO";
	
	@Override
	public Address load(Address e) {
		return getSqlSession().selectOne(className + ".load", e);
	}

	@Override
	public Address getByUser(String addressId, String userId) {
		ParameterMap params = new ParameterMap();
		params.put("addressId", addressId);
		params.put("userId", userId);
		return getSqlSession().selectOne(className + ".getByUser", params);
	}

	@Override
	public List<Address> listByUser(String userId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		return getSqlSession().selectList(className + ".list", params);
	}

}
