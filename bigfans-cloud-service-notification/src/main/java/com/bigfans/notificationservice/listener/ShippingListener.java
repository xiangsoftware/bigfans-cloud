package com.bigfans.notificationservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.framework.plugins.SmsPlugin;
import com.bigfans.model.event.shipping.ShippingCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lichong
 * @create 2018-03-25 上午7:49
 **/
@Component
@KafkaConsumerBean
public class ShippingListener {

    @Autowired
    private SmsPlugin smsPlugin;

    @KafkaListener
    public void on(ShippingCreatedEvent event){

    }

}
