package com.bigfans.model.dto.order;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-13 下午8:27
 **/
@Data
public class OrderItemPricingDto {

    private String prodId;
    private Integer quantity;
}
