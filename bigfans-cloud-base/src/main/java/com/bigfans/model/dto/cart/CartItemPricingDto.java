package com.bigfans.model.dto.cart;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lichong
 * @create 2018-02-16 下午5:57
 **/
@Data
public class CartItemPricingDto {

    private String prodId;
    private BigDecimal originalPrice;
    private BigDecimal price;
    private BigDecimal originalSubTotal;
    private BigDecimal subtotal;

}
