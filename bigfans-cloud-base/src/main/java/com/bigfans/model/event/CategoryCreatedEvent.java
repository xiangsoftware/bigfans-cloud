package com.bigfans.model.event;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-05 下午8:08
 **/
@Data
public class CategoryCreatedEvent extends AbstractEvent{

    public CategoryCreatedEvent() {
    }

    public CategoryCreatedEvent(String id) {
        this.id = id;
    }

    protected String id;

}
