package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.exception.UsePropertyException;
import com.bigfans.orderservice.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class UserServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Void> useProperty(String orderId, String couponId, Float points, BigDecimal balance) {
        String userToken = CookieHolder.getValue(Constants.TOKEN.KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            HttpHeaders headers = new HttpHeaders();
            headers.put(Constants.TOKEN.HEADER_KEY_NAME, Arrays.asList(Constants.TOKEN.KEY_NAME + "=" + userToken));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

            Map<String, Object> payload = new HashMap<>();
            payload.put("id", orderId);
            payload.put("couponId", couponId);
            payload.put("points", points);
            payload.put("balance", balance);

            HttpEntity requestEntity = new HttpEntity(payload, headers);

            UriComponents builder = UriComponentsBuilder.fromUriString("http://user-service/useProperty").build().expand().encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.exchange(builder.toUri(), HttpMethod.GET, requestEntity, RestResponse.class);
            if(!responseEntity.getStatusCode().is2xxSuccessful()){
                throw new UsePropertyException();
            }
            return null;
        });
    }

    public CompletableFuture<Address> myAddress(String addressId) {
        String userToken = CookieHolder.getValue(Constants.TOKEN.KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            HttpHeaders headers = new HttpHeaders();
            headers.put(Constants.TOKEN.HEADER_KEY_NAME, Arrays.asList(Constants.TOKEN.KEY_NAME + "=" + userToken));
            HttpEntity requestEntity = new HttpEntity(null, headers);
            UriComponents builder = UriComponentsBuilder.fromUriString("http://user-service/myAddress/{addressId}").build().expand(addressId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.exchange(builder.toUri(), HttpMethod.GET, requestEntity, RestResponse.class);
            RestResponse restResponse = responseEntity.getBody();
            Map data = (Map) restResponse.getData();
            Address address = BeanUtils.mapToModel(data, Address.class);
            return address;
        });
    }
}
